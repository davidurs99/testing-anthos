#!/bin/sh

export CLUSTER1="testing-anthos"
export CLUSTER2="gce.k8s.local" # The cluster name - needs x.y.local

export CLUSTER_ZONE="europe-west2-a"
export NODE_SIZE="e2-standard-4" # needed for istio
export G_ACCOUNT=`gcloud config get-value account`
export PROJECT=`gcloud config get-value project`

export CTX_CLUSTER1=${CLUSTER1}
export CTX_CLUSTER2=${CLUSTER2}

gcloud projects add-iam-policy-binding ${PROJECT} --member "user:${G_ACCOUNT}" --role roles/gkehub.admin \
  --role roles/iam.serviceAccountAdmin --role roles/iam.serviceAccountKeyAdmin --role roles/resourcemanager.projectIamAdmin


## CREATE "ON-PREM" ClUSTER ON GCE
export KOPS_FEATURE_FLAGS=AlphaAllowGCE
export KOPS_STATE_STORE=gs://testing-anthos
kops create cluster --name=${CLUSTER2} --zones=${CLUSTER_ZONE} --project=${PROJECT} \
  --node-count=2 --node-size=${NODE_SIZE} --admin-access=0.0.0.0/0 --yes

## CREATE GKE CLUSTER

gcloud container clusters create ${CLUSTER1} --zone ${CLUSTER_ZONE} --username admin \
  --machine-type ${NODE_SIZE} --num-nodes 4  --min-nodes 2 --max-nodes 4 --network default --enable-ip-alias 


kubectl --context=${CTX_CLUSTER1} create clusterrolebinding user-cluster-admin --clusterrole cluster-admin --user ${G_ACCOUNT} # to ensure cluster-admin access
kubectl --context=${CTX_CLUSTER2} create clusterrolebinding user-cluster-admin --clusterrole cluster-admin --user ${G_ACCOUNT} # to ensure cluster-admin access


## REGISTER CLUSTERS

gcloud iam service-accounts create anthos-connect --project=${PROJECT}
gcloud projects add-iam-policy-binding ${PROJECT} \
  --member="serviceAccount:anthos-connect@${PROJECT}.iam.gserviceaccount.com" --role="roles/gkehub.connect"
gcloud iam service-accounts keys create anthos-connect.json \
  --iam-account=anthos-connect@${PROJECT}.iam.gserviceaccount.com --project=${PROJECT}


gcloud container hub memberships register ${CLUSTER1} --gke-cluster=${CLUSTER_ZONE}/${CLUSTER1} \
  --service-account-key-file=anthos-connect.json --project=${PROJECT}

KUBECONFIG= kubectl config view --minify --flatten --context=${CTX_CLUSTER2} > $(pwd)/${CTX_CLUSTER2}.context
gcloud container hub memberships register remote-gce --context=${CTX_CLUSTER2} \
  --service-account-key-file=anthos-connect.json --kubeconfig=${CTX_CLUSTER2}.context --project=${PROJECT}

### LOGIN REMOTE CLUSTER

kubectl --context=${CTX_CLUSTER2} create serviceaccount remote-admin-sa
kubectl --context=${CTX_CLUSTER2} create clusterrolebinding ksa-admin-binding --clusterrole cluster-admin --serviceaccount default:remote-admin-sa
SECRET_NAME=$(kubectl --context=$CTX_CLUSTER2 get serviceaccount remote-admin-sa -o jsonpath='{$.secrets[0].name}')
kubectl --context=${CTX_CLUSTER2}  get secret ${SECRET_NAME} -o jsonpath='{$.data.token}' | base64 --decode # Login with this secret on GCP UI


## ASM 


curl -LO https://storage.googleapis.com/gke-release/asm/istio-1.8.3-asm.2-osx.tar.gz
curl -LO https://storage.googleapis.com/gke-release/asm/istio-1.8.3-asm.2-osx.tar.gz.1.sig
openssl dgst -sha256 -verify /dev/stdin -signature istio-1.8.3-asm.2-osx.tar.gz.1.sig istio-1.8.3-asm.2-osx.tar.gz <<'EOF'
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEWZrGCUaJJr1H8a36sG4UUoXvlXvZ
wQfk16sxprI2gOJ2vFFggdq3ixF2h4qNBt0kI7ciDhgpwS8t+/960IsIgw==
-----END PUBLIC KEY-----
EOF

tar xzf istio-1.8.3-asm.2-osx.tar.gz
cd istio-1.8.3-asm.2
export PATH=$PWD/bin:$PATH

### Install CERTS

mkdir -p certs && \
pushd certs
make -f ../tools/certs/Makefile.selfsigned.mk root-ca
make -f ../tools/certs/Makefile.selfsigned.mk all-cacerts

kubectl --context="${CTX_CLUSTER1}" create namespace istio-system
kubectl --context="${CTX_CLUSTER1}" create secret generic cacerts -n istio-system \
  --from-file=all/ca-cert.pem \
  --from-file=all/ca-key.pem \
  --from-file=all/root-cert.pem \
  --from-file=all/cert-chain.pem


kubectl --context="${CTX_CLUSTER2}" create namespace istio-system
kubectl --context="${CTX_CLUSTER2}" create secret generic cacerts -n istio-system \
  --from-file=all/ca-cert.pem \
  --from-file=all/ca-key.pem \
  --from-file=all/root-cert.pem \
  --from-file=all/cert-chain.pem

popd

### Setting the default network

NETWORK1="network1"
NETWORK2="network2"

kubectl --context="${CTX_CLUSTER1}" label namespace istio-system topology.istio.io/network=${NETWORK1}
kubectl --context="${CTX_CLUSTER2}" label namespace istio-system topology.istio.io/network=${NETWORK2}



### Install the mesh

export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT} --format="value(projectNumber)")
export MESH_ID="proj-${PROJECT_NUMBER}"
export ASM_VERSION="asm-183-2"

kpt pkg get \
https://github.com/GoogleCloudPlatform/anthos-service-mesh-packages.git/asm@release-1.8-asm asm

kpt cfg set asm gcloud.core.project ${PROJECT}
kpt cfg set asm gcloud.project.environProjectNumber ${PROJECT_NUMBER}
kpt cfg set asm gcloud.container.cluster ${CLUSTER1}
kpt cfg set asm gcloud.compute.location ${CLUSTER_ZONE}
kpt cfg set asm anthos.servicemesh.rev asm-183-2

istioctl install -y --context="${CTX_CLUSTER1}" \
  -f asm/istio/istio-operator.yaml \
  -f asm/istio/options/citadel-ca.yaml \
  -f asm/istio/options/multiproject.yaml \
  -f asm/istio/options/multicluster.yaml \
  --revision=${ASM_VERSION}

cat <<EOF > istio-operator2.yaml
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
spec:
  profile: asm-multicloud
  revision: ${ASM_VERSION}
  values:
    global:
      meshID: ${MESH_ID}
      multiCluster:
        clusterName: ${CLUSTER2}
      network: ${NETWORK2}
EOF

istioctl install -y --context="${CTX_CLUSTER2}" -f istio-operator2.yaml



## EAST WEST GATEWAY

asm/istio/expansion/gen-eastwest-gateway.sh \
    --mesh ${MESH_ID} --cluster ${CLUSTER1} --network ${NETWORK1} \
    --revision ${ASM_VERSION} > east-west1.yaml

asm/istio/expansion/gen-eastwest-gateway.sh \
    --mesh ${MESH_ID} --cluster ${CLUSTER2} --network ${NETWORK2} \
    --revision ${ASM_VERSION} > east-west2.yaml
  



istioctl --context="${CTX_CLUSTER1}" install -y -f east-west1.yaml
istioctl --context="${CTX_CLUSTER2}" install -y -f east-west2.yaml

kubectl --context="${CTX_CLUSTER1}" apply -n istio-system -f asm/istio/expansion/expose-services.yaml
kubectl --context="${CTX_CLUSTER2}" apply -n istio-system -f asm/istio/expansion/expose-services.yaml

istioctl x create-remote-secret \
  --context="${CTX_CLUSTER1}" \
  --name=cluster1 | \
  kubectl apply -f - --context="${CTX_CLUSTER2}"

istioctl x create-remote-secret \
  --context="${CTX_CLUSTER2}" \
  --name=cluster2 | \
  kubectl apply -f - --context="${CTX_CLUSTER1}"


### Verify instalation

kubectl create --context="${CTX_CLUSTER1}" namespace sample
kubectl create --context="${CTX_CLUSTER2}" namespace sample
kubectl label --context="${CTX_CLUSTER1}" namespace sample \
    istio.io/rev=${ASM_VERSION}
kubectl label --context="${CTX_CLUSTER2}" namespace sample \
    istio.io/rev=${ASM_VERSION}

kubectl apply --context="${CTX_CLUSTER1}" \
    -f samples/helloworld/helloworld.yaml \
    -l service=helloworld -n sample
kubectl apply --context="${CTX_CLUSTER2}" \
    -f samples/helloworld/helloworld.yaml \
    -l service=helloworld -n sample

kubectl apply --context="${CTX_CLUSTER1}" \
    -f samples/helloworld/helloworld.yaml \
    -l version=v1 -n sample

kubectl apply --context="${CTX_CLUSTER2}" \
    -f samples/helloworld/helloworld.yaml \
    -l version=v2 -n sample

kubectl apply --context="${CTX_CLUSTER1}" \
    -f samples/sleep/sleep.yaml -n sample
kubectl apply --context="${CTX_CLUSTER2}" \
    -f samples/sleep/sleep.yaml -n sample

#### Run the request several times, and should receive different versions
kubectl exec --context="${CTX_CLUSTER1}" -n sample -c sleep \
    "$(kubectl get pod --context="${CTX_CLUSTER1}" -n sample -l \
    app=sleep -o jsonpath='{.items[0].metadata.name}')" \
    -- curl -sS helloworld.sample:5000/hello


#### Test the same from different cluster

kubectl exec --context="${CTX_CLUSTER2}" -n sample -c sleep \
    "$(kubectl get pod --context="${CTX_CLUSTER2}" -n sample -l \
    app=sleep -o jsonpath='{.items[0].metadata.name}')" \
    -- curl -sS helloworld.sample:5000/hello



## DEPLOY CONFIG MANAGEMENT

cd ..

kubectl --context=${CTX_CLUSTER1} create ns config-management-system && \
kubectl --context=${CTX_CLUSTER1} create secret generic git-creds --namespace=config-management-system --from-file=ssh=$(realpath ~/.ssh/gcp) 

kubectl --context=${CTX_CLUSTER1} apply -f config-management-operator.yaml
kubectl --context=${CTX_CLUSTER1} apply -f config-management-cluster1.yaml


kubectl --context=${CTX_CLUSTER2} create ns config-management-system && \
kubectl --context=${CTX_CLUSTER2} create secret generic git-creds --namespace=config-management-system --from-file=ssh=$(realpath ~/.ssh/gcp) 

kubectl --context=${CTX_CLUSTER2} apply -f config-management-operator.yaml
kubectl --context=${CTX_CLUSTER2} apply -f config-management-cluster2.yaml





## CLEANUP

cd ..
rm -rf istio-1.8.3-asm.2
rm istio*.tar.gz*
rm anthos-connect.json

gcloud container clusters delete ${CLUSTER1} --zone ${CLUSTER_ZONE}
gcloud container hub memberships delete ${CLUSTER1}
kops delete cluster ${CLUSTER2} --yes
gcloud container hub memberships delete remote-gce