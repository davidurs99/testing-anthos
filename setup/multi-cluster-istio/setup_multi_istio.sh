#!/bin/sh

ISTIO_PATH="../istio-1.9.0"

CLUSTER_1="testing-anthos"
CLUSTER_2="gce.k8s.local"


cd $ISTIO_PATH

mkdir -p certs && \
pushd certs
make -f ../tools/certs/Makefile.selfsigned.mk root-ca
make -f ../tools/certs/Makefile.selfsigned.mk cluster1-cacerts
CTX_CLUSTER1=${CLUSTER_1}
CTX_CLUSTER2=${CLUSTER_2}
kubectl --context="${CTX_CLUSTER1}" create namespace istio-system
kubectl --context="${CTX_CLUSTER1}" create secret generic cacerts -n istio-system \
  --from-file=cluster1/ca-cert.pem \
  --from-file=cluster1/ca-key.pem \
  --from-file=cluster1/root-cert.pem \
  --from-file=cluster1/cert-chain.pem

kubectl --context="${CTX_CLUSTER2}" create namespace istio-system
kubectl --context="${CTX_CLUSTER2}" create secret generic cacerts -n istio-system \
  --from-file=cluster1/ca-cert.pem \
  --from-file=cluster1/ca-key.pem \
  --from-file=cluster1/root-cert.pem \
  --from-file=cluster1/cert-chain.pem

popd

kubectl --context="${CTX_CLUSTER1}" get namespace istio-system && \
  kubectl --context="${CTX_CLUSTER1}" label namespace istio-system topology.istio.io/network=network1

kubectl --context="${CTX_CLUSTER2}" get namespace istio-system && \
  kubectl --context="${CTX_CLUSTER2}" label namespace istio-system topology.istio.io/network=network2


istioctl install --context="${CTX_CLUSTER1}" -f istio-operator1.yaml
istioctl install --context="${CTX_CLUSTER2}" -f istio-operator2.yaml

istioctl --context="${CTX_CLUSTER1}" install -y -f east-west1.yaml
istioctl --context="${CTX_CLUSTER2}" install -y -f east-west2.yaml

kubectl --context="${CTX_CLUSTER1}" apply -n istio-system -f \
    ${ISTIO_PATH}/samples/multicluster/expose-services.yaml

kubectl --context="${CTX_CLUSTER2}" apply -n istio-system -f \
    ${ISTIO_PATH}/samples/multicluster/expose-services.yaml
    


istioctl x create-remote-secret \
  --context="${CTX_CLUSTER1}" \
  --name=cluster1 | \
  kubectl apply -f - --context="${CTX_CLUSTER2}"


istioctl x create-remote-secret \
  --context="${CTX_CLUSTER2}" \
  --name=cluster2 | \
  kubectl apply -f - --context="${CTX_CLUSTER1}"


